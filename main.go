package main

import (
	"bytes"
	"os"

	"github.com/alexflint/go-arg"
)

const (
	ErrReadingConfig = 1
	ErrReadingFile   = 2
	ErrOpenURL       = 3
	ErrOpeningFile   = 4
)

type CLIArgs struct {
	Config           string `arg:"-c, --config" default:"webcrawler.toml"`
	LoginCredentials string `arg:"-l, --login" default:"credentials.json"`
	In               string `arg:"-i, --in" help:"path of the input .html file if the webcrawler is not used"`
	Out              string `arg:"-o, --out" default:"students.csv"`
}

func main() {

	args := CLIArgs{}
	arg.MustParse(&args)

	// webcrawler, _ := NewWebCrawler(args.Config, args.LoginCredentials)
	// crawl(webcrawler, &args)
	var html string

	if len(args.In) == 0 {
		webcrawler, err := NewWebCrawler(args.Config, args.LoginCredentials)
		if err != nil {
			println(err.Error())
			os.Exit(ErrReadingConfig)
		}
		err = webcrawler.OpenURL(webcrawler.URL)
		if err != nil {
			println(err.Error())
			os.Exit(ErrOpenURL)
		}

		html, err = webcrawler.DownloadHTML()
		if err != nil {
			println(err.Error())
			os.Exit(ErrOpenURL)
		}
	}

	if len(args.In) > 0 {
		file, err := os.Open(args.In)
		if err != nil {
			println(err.Error())
			os.Exit(ErrOpeningFile)
		}

		buffer := new(bytes.Buffer)
		_, err = buffer.ReadFrom(file)
		if err != nil {
			println(err.Error())
			os.Exit(ErrReadingFile)
		}

		html = buffer.String()

	}

	html2csv, err := NewHTML2CSV(args.Config)
	if err != nil {
		println(err.Error())
		os.Exit(ErrReadingConfig)
	}
	err = html2csv.CreateCSVFromHTML(html, args.Out)
	if err != nil {
		println(err.Error())
		os.Exit(ErrOpeningFile)
	}

}
