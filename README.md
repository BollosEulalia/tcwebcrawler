# TcWebcrawler

Teachcenter webcrawler. A CLI-based tool to create a template csv for the [repomanager](https://gitlab.com/BollosEulalia/repomanager)
It is able to extract a student list and convert it to html or convert an html file containing a table to a csv.


## Description
This is a webcrawler and html-table-to-csv converter.
The latter feature is impleneted because the webcrawler breaks approximately each semester start because of teachcenter updates.


## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

There is no installation required. Just use the binary for your operating system.

## Usage

If the page requires a login create a file called `credentials.json`.
You can use `test/testdata/credentials.json` as template.

Start with the provided configuration file at `test/testdata/webcrawler.toml` and adjust the values.

Adjust the values according to your html table. If your table doesn't provide columns like *Role* try leaving it empty.

If the table ID is not *participants* change the value in webcrawler.toml

Run with `./tcwebcrawler`


### If the webcrawler is broken

1. Navigate to the URL containing the participants table. 
2. Check that all participants are visible.
3. Press `CTRL+S`. 
4. Select `Web Page, HTML only` and save the file.
	1. See image below.
5. Run `./tcwebcrawler --in participants.html --out repolist.csv`

![Download Participants HTML Table](images/download.png)


## Support
Please create an issue.

## Roadmap

Plans: GUI

## Contributing

**Requirements:**

* Go
	- Required dependencies should be downloaded automatically e.g. if you compile the project.

If you have some good ideas please create an issue.

If you want to implement a new feature please contact me before creating a pull request. (e.g. to avoid that I'm currently working on the same feature)


## License
GNU GPL V3

## Project status

Currently in active development.
