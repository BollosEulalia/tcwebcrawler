module gitlab.com/BollosEulalia/tcwebcrawler

go 1.18

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/alexflint/go-arg v1.4.3 // indirect
	github.com/alexflint/go-scalar v1.1.0 // indirect
	github.com/anaskhan96/soup v1.2.5 // indirect
	golang.org/x/net v0.0.0-20220403103023-749bd193bc2b // indirect
	golang.org/x/text v0.3.7 // indirect
)
