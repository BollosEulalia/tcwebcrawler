package main

import "testing"

func TestNewWebCrawler(t *testing.T) {

	want := []string{"https://tc.tugraz.at/main/login/index.php",
		"https://tc.tugraz.at/main/user/index.php?id=2295",
		"&perpage=all",
		"participants"}

	webcrawler, err := NewWebCrawler("test/testdata/config.toml", "test/testdata/credentials.json")
	if err != nil {
		t.Errorf("error: %v\n", err)
	}

	in := []string{webcrawler.LoginURL,
		webcrawler.URL,
		webcrawler.URLAppend,
		webcrawler.TableID}

	for i := range want {
		if in[i] != want[i] {
			t.Errorf("TestNewWebCrawler. in= %q\nwant= %q\n%v", want[i], in[i], want)
		}
	}

}
