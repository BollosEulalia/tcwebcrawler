package main

import (
	"errors"
	"os"
	"path/filepath"
	"strings"

	"github.com/anaskhan96/soup"
)

type HTML2CSV struct {
	CSVDelimiter   string
	ExcludedGroups []string
	ExcludedRoles  []string
	IncludedRoles  []string
	IncludedGroups []string
	// htmlTableRequiredColumns []string
	tableID string
}

func NewHTML2CSV(configFilename string) (*HTML2CSV, error) {
	cfg, err := ReadConfig(configFilename)
	if err != nil {
		return nil, err
	}

	html2csv := cfg.HTML2CSV
	html2csv.tableID = cfg.WebCrawler.TableID
	// html2csv.htmlTableRequiredColumns = []string{"Name", "MatriculationNumber", "Email", "Groups", "Rolls"}

	return html2csv, err

}

func (html2csv *HTML2CSV) readConfig(filename string) error {

	cfg, err := ReadConfig(filename)
	if err != nil {
		return err
	}

	html2csv = cfg.HTML2CSV

	return nil
}

func (html2csv *HTML2CSV) CreateCSVFromHTML(html string, outputFilename string) error {

	data := html2csv.htmlTo2DArray(html)
	if data == nil {
		return errors.New("could not extract data from html. Unable to create database")
	}

	students, err := html2csv.cleanData(data)
	if err != nil {
		return err
	}

	return html2csv.createCSVFile(outputFilename, students)

	// return nil

}

func (html2csv *HTML2CSV) createCSVFile(filename string, students [][]string) error {

	err := os.MkdirAll(filepath.Dir(filename), os.ModePerm)
	if err != nil {
		return err
	}

	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	for i := range students {
		_, err = file.WriteString(strings.Join(students[i], html2csv.CSVDelimiter) + "\n")
		if err != nil {
			return err
		}
	}

	println("Created file ", filename)

	return nil
}

//cleanData removes unused columns from given array and returns a new clean 2d string array
func (html2csv *HTML2CSV) cleanData(data [][]string) ([][]string, error) {

	students := make([][]string, 1, len(data))
	roleColumn := -1
	groupColumn := -1

	students[0] = []string{"Name", "MatriculationNr", "Email", "Groups", "Roles"} // html2csv.htmlTableRequiredColumns
	// substringsToCheck := []string{"name", "nummer", "number", "grupp", "group", "roll", "role"}
	header := make(map[string]int)

	for i := range data[0] {
		col := strings.ToLower(data[0][i])
		//for _, usedColumn := range substringsToCheck {
		//currentUsedCol := strings.ToLower(usedColumn)
		//fmt.Printf("current col: %v; checking: %v\n", col, usedColumn)
		//if strings.Contains(strings.ToLower(col), currentUsedCol) {

		if strings.Contains(col, "name") {
			header["Name"] = i
		} else if strings.Contains(col, "nummer") || strings.Contains(col, "number") {
			header["MatriculationNr"] = i
		} else if strings.Contains(col, "mail") {
			header["Email"] = i
		} else if strings.Contains(col, "grupp") || strings.Contains(col, "group") {
			header["Groups"] = i
			groupColumn = i
		} else if strings.Contains(col, "roll") || strings.Contains(col, "role") {
			header["Rolls"] = i
			roleColumn = i
		}
		//}
		//}
	}

	if roleColumn == -1 {
		return nil, errors.New("column for roles could not be found")
	} else if groupColumn == -1 {
		return nil, errors.New("group column could not be found")
	}

	for _, rowData := range data[1:] {
		if html2csv.isRoleValid(rowData[roleColumn]) && html2csv.isGroupValid(rowData[groupColumn]) {
			temp := make([]string, len(students[0]))

			for col, columnName := range students[0] {
				temp[col] = strings.TrimSpace(rowData[header[columnName]])
			}

			students = append(students, temp)

		}
	}

	return students, nil
}

func (html2csv *HTML2CSV) isRoleValid(roles string) bool {
	return html2csv.isAttributeValid(roles, html2csv.ExcludedRoles, html2csv.IncludedRoles)
}

func (html2csv *HTML2CSV) isGroupValid(groups string) bool {
	return html2csv.isAttributeValid(groups, html2csv.ExcludedGroups, html2csv.IncludedGroups)
}

func (html2csv *HTML2CSV) isAttributeValid(attribute string, invalidValues []string, validValues []string) bool {

	for _, excludedGroup := range invalidValues {
		if strings.Contains(attribute, excludedGroup) {
			return false
		}
	}
	for _, includedGroup := range validValues {
		if strings.Contains(attribute, includedGroup) {
			return true
		}
	}

	return false
}

func (html2csv *HTML2CSV) htmlTo2DArray(html string) [][]string {

	var data [][]string
	table := soup.HTMLParse(html).Find("table", "id", html2csv.tableID)

	var row []string

	for _, column := range table.Find("thead").FindAll("th") {
		row = append(row, strings.TrimSpace(column.FullText()))
	}

	data = append(data, row)

	for _, rowData := range table.Find("tbody").FindAll("tr") {
		row = make([]string, 0)
		for _, columns := range rowData.Children() {
			row = append(row, strings.TrimSpace(columns.FullText()))
		}
		data = append(data, row)
	}

	return data
}

func ArrayContainsSubstring(array []string, substring string) bool {
	for _, str := range array {
		if strings.Contains(str, substring) {
			return true
		}
	}
	return false
}
