package main

import "testing"

func TestReadConfig(t *testing.T) {
	_, err := ReadConfig("test/testdata/config.toml")

	if err != nil {
		t.Errorf("err=%q\n", err)
	}

}
