package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"net/http/cookiejar"
	"net/url"

	"github.com/anaskhan96/soup"
)

// type LoginData struct {
// 	Password string
// 	Username string
// }

type WebCrawler struct {
	client           *http.Client
	response         *http.Response
	cookies          *http.CookieJar
	request          *http.Request
	MaxLoginAttempts int
	TableID          string
	URL              string
	URLAppend        string
	LoginURL         string
	loginData        url.Values
	config           string
	responseBody     string
}

func NewWebCrawler(configFilename string, credentialsFilename string) (*WebCrawler, error) {

	jar, err := cookiejar.New(nil)

	if err != nil {
		println("Could not init cookies for webcrawler.")
		return nil, err
	}

	cfg, err := ReadConfig(configFilename)
	if err != nil {
		return nil, err
	}

	crawler := cfg.WebCrawler

	crawler.client = &http.Client{Jar: jar}
	crawler.loginData = url.Values{}
	crawler.MaxLoginAttempts = int(math.Max(1, float64(crawler.MaxLoginAttempts)))
	crawler.readCredentials(credentialsFilename)
	crawler.URL = crawler.URL + crawler.URLAppend

	return crawler, nil

}

func (crawler *WebCrawler) readConfig(filename string) error {

	cfg, err := ReadConfig(filename)
	if err != nil {
		return err
	}

	crawler = cfg.WebCrawler

	return nil
}

func (crawler *WebCrawler) readCredentials(filename string) error {

	var loginData map[string]string = make(map[string]string)

	fileData, err := ioutil.ReadFile(filename)

	if err != nil {
		println("Could not read credentials file")
		return err
	}

	err = json.Unmarshal(fileData, &loginData)
	if err != nil {
		return err
	}

	for key, value := range loginData {
		crawler.loginData.Add(key, value)
	}

	return nil
}

func (crawler *WebCrawler) doRequest(method string, url string) error {

	var err error = nil

	crawler.request, err = http.NewRequest(method, url, nil)

	if err != nil {
		return err
	}

	crawler.request.Header.Set("Connection", "Keep-Alive")
	crawler.request.Header.Set("Accept-Language", "en-US")
	crawler.request.Header.Set("User-Agent", "Mozilla/5.0")

	crawler.response, err = crawler.client.Do(crawler.request)
	if err != nil {
		return err
	}

	defer crawler.response.Body.Close()

	crawler.responseBody, err = crawler.responseToString(crawler.response)
	return err
	// return nil
}

func (crawler *WebCrawler) responseToString(resp *http.Response) (string, error) {

	buffer := new(bytes.Buffer)
	_, err := buffer.ReadFrom(resp.Body)
	if err != nil {
		return "", fmt.Errorf("could not parse html from %v", resp.Request.URL.String())
	}
	str := buffer.String()
	return str, nil
}

func (crawler *WebCrawler) PressContinueButton() error {
	println("Pressing button (javascript disabled needs an additional button press)")

	postData := url.Values{}

	toFind := []string{"body", "form", "div"}
	var form soup.Root

	html := soup.HTMLParse(crawler.responseBody)
	if html.Error != nil {
		return fmt.Errorf("could not parse html at %v, %v", crawler.response.Request.URL.String(), html.Error)
	}

	for _, find := range toFind {
		html = html.Find(find)
		if html.Error != nil {
			return fmt.Errorf("%v not found at %v, %v", find,
				crawler.response.Request.URL.String(), html.Error)
		}
		if find == "form" {
			form = soup.HTMLParse(html.HTML()).Find("form")
		}
	}

	var postLink string

	for key, value := range form.Attrs() {
		if key == "action" {
			// postData.Add(key, value)
			postLink = value
		}
	}

	for _, child := range html.FindAll("input") {
		attrs := child.Attrs()

		value, ok2 := attrs["value"]
		if name, ok := attrs["name"]; ok && ok2 {
			postData.Add(name, value)
		}

	}
	var err error = nil

	fmt.Printf("posting to %v\n", postLink)
	// crawler.response, err = crawler.client.PostForm(crawler.response.Request.URL.String(), postData)
	crawler.response, err = crawler.client.PostForm(postLink, postData)
	if err != nil {
		println("button press error")
		return err
	}

	fmt.Printf("post-button url = %v\n", crawler.response.Request.URL.String())

	return nil
}

func (crawler *WebCrawler) Login() error {

	err := crawler.doRequest("GET", crawler.LoginURL)
	if err != nil {
		return err
	}

	println("Posting login data to ", crawler.response.Request.URL.String())
	crawler.response, err = crawler.client.PostForm(crawler.response.Request.URL.String(), crawler.loginData)

	if err != nil {
		println("login error")
		return err
	}
	crawler.responseBody, err = crawler.responseToString(crawler.response)
	if err != nil {
		return err
	}

	return crawler.PressContinueButton()

}

func (crawler *WebCrawler) OpenURL(url string) error {

	err := crawler.doRequest("GET", url)

	if err != nil || crawler.response == nil {
		return err
	}

	defer crawler.response.Body.Close()

	for i := 0; i < crawler.MaxLoginAttempts && crawler.response.Request.URL.String() != url; i++ {
		err = crawler.Login()
		if err != nil {
			return err
		}

		err = crawler.doRequest("GET", url)
		if err != nil {
			return err
		}
		// if crawler.response.Request.URL.String() != crawler.URL {
		// 	err = crawler.doRequest("GET", crawler.LoginURL)
		// }
		fmt.Printf("current url: %v\n", crawler.response.Request.URL.String())
	}

	return nil
}

//DownloadHTML downloads the website opened with OpenURL.
func (crawler *WebCrawler) DownloadHTML() (string, error) {

	println("converting html to string")
	// buffer := new(bytes.Buffer)
	// _, err := buffer.ReadFrom(crawler.responseBody)
	// if err != nil {
	// 	return "", err
	// }

	// return buffer.String(), nil
	return crawler.responseBody, nil
}
